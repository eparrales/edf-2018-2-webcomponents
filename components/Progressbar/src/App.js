window.customElements.define('progress-bar', Progressbar);

/**
 * Ojo que es diferente el uso de (function(){}); y de document.body.onload
 * mientras que (function(){}) es la ejecución anónima de un script en tiempo
 * de ejecución, el otro es un listener para ejecutarse cuando el cuerpo esté
 * listo.
 */
document.body.onload = function(){
    console.log("Body is ready");
};

(function() { 
    
    console.log("Executing script");

    var progress = document.querySelector('progress-bar'),
        complete = 0;
    
    //console.log(progress);

    var progressInterval = setInterval(() => {
      complete += 1;
      if (complete <= 100) {
        progress.setAttribute('complete', complete);
        //console.log(`${complete}%`);
      } else {
        clearInterval(progressInterval);
      }
    }, 100);
})();

