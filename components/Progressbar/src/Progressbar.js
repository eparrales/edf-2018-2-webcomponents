class Progressbar extends HTMLElement {
    constructor() {
        super();
        this._shadow = this.createShadowRoot();
        this._complete = 0;
    }

    get shadow() {
        return this._shadow;
    }

    set shadow(val) {
        this._shadow = val;
    }

    get complete() {
        return this._complete;
    }

    set complete(val) {
        this._complete = val;
    }

    /**
     * observedAttributes
     * 
     * Registra los atributos del componente (atributos del tag que representa el componente)
     * para observar sus cambios de valor en el DOM.
     */
    static get observedAttributes() {
        return ['complete', 'color'];
    }

    /**
     * attributeChangedCallback
     * 
     * Se ejecuta cuando el valor de cualquier atributo declarado dentro del arreglo de los 
     * observedAttributes cambia.
     * 
     * @param {string} name nombre del atributo que cambia
     * @param {mixed} oldVal valor anterior del atributo
     * @param {mixed} newValue nuevo valor del atributo
     */
    attributeChangedCallback(name, oldVal, newValue) {
        this[`update${name.charAt(0).toUpperCase() + name.slice(1)}`](oldVal,newValue);
    }

    updateComplete(oldVal, newValue) {
        var innerBar = this.shadow.querySelector('.progress-bar-inner');
        this._complete = parseInt(newValue, 10) || 0;
        innerBar.style.width = this.complete + '%';
        innerBar.innerHTML = this.complete + '%';
    }

    updateColor(oldVal, newValue) {
        var innerBar = this.shadow.querySelector('.progress-bar-inner');
        innerBar.style.background = newValue;
    }

    /**
     * connectedCallback
     * 
     * Se llama una vez el elemento se ha integrado al DOM, pues este es un custom element
     * y se usa para asegurarse de ejecutarse cuando ya puede ser modificado, así que acá
     * sí podemos darle valor a sus atributos.
     * 
     * constructor() is called when the element is created.
     * connectedCallback() is called when (after) the element is attached to the DOM.
     * 
     * https://stackoverflow.com/questions/40492330/difference-between-constructor-and-connectedcallback-in-custom-elements-v1
     * 
     */
    connectedCallback() {
        var template = `
            <style>
                .progress-bar {
                    width: 50%;
                    height: 30px;
                    background-color: #EDF2F4;
                    border-radius: 5px;
                    color: #FFF;
                }

                .progress-bar-inner {
                    height: 100%;
                    line-height: 30px;
                    background: #2B2D42;
                    text-align: center;
                    border-radius: 5px;
                    transition: width 0.25s;
                }
            </style>
            <div class="progress-bar">
                <div class="progress-bar-inner">${this.complete}%</div>
            </div>
        `;

        this.shadow.innerHTML = template;
    }
}